defmodule ElixiratorTest do
  use ExUnit.Case
  doctest Elixirator

  test "calculate fuel for landing" do
    assert Elixirator.calculate_total_fuel(28_801, [{:land, 9.807}]) == 13_447
    assert Elixirator.calculate_total_fuel(28_801, [{:land, 1.62}]) == 1535
  end

  test "calculate fuel for launching" do
    assert Elixirator.calculate_total_fuel(28_801, [{:launch, 9.807}]) == 19_772
    assert Elixirator.calculate_total_fuel(28_801, [{:launch, 1.62}]) == 2024
  end

  test "calculate fuel for path" do
    assert Elixirator.calculate_total_fuel(28_801, [{:launch, 9.807}, {:land, 1.62}, {:launch, 1.62}, {:land, 9.807}]) == 51_898
    assert Elixirator.calculate_total_fuel(14_606, [{:launch, 9.807}, {:land, 3.711}, {:launch, 3.711}, {:land, 9.807}]) == 33_388
    assert Elixirator.calculate_total_fuel(75_432, [{:launch, 9.807}, {:land, 1.62}, {:launch, 1.62}, {:land, 3.711}, {:launch, 3.711}, {:land, 9.807}]) == 212_161
  end
end
