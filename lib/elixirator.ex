defmodule Elixirator do
  @doc """
  calculates fuel to
  launch from one planet of the Solar system, and to land on another planet of the Solar system,
  depending on the flight route.

  arguments example: 28801, [{:launch, 9.807}, {:land, 1.62}, {:launch, 1.62}, {:land, 9.807}]
  """
  @spec calculate_total_fuel(number(), list({:launch | :land, float()})) :: non_neg_integer()
  def calculate_total_fuel(equipment_weight, flight_steps) do
    flight_steps
    |> Enum.reverse()
    |> Enum.reduce(0, fn {land_or_launch, gravity}, acc ->
      acc + calculate_flight_step_fuel(equipment_weight + acc, land_or_launch, gravity, 0)
    end)
  end

  @spec calculate_flight_step_fuel(number(), atom(), float(), non_neg_integer()) :: non_neg_integer()
  defp calculate_flight_step_fuel(weight, _land_or_launch, _gravity, total) when weight <= 0, do: total

  defp calculate_flight_step_fuel(weight, land_or_launch, gravity, total) do
    fuel_weight = get_fuel_weight(weight, land_or_launch, gravity)

    calculate_flight_step_fuel(fuel_weight, land_or_launch, gravity, total + fuel_weight)
  end

  @spec get_fuel_weight(number(), atom(), float()) :: non_neg_integer()
  defp get_fuel_weight(weight, :launch, gravity) do
    weight = trunc(weight * gravity * 0.042 - 33)
    max(weight, 0)
  end

  defp get_fuel_weight(weight, :land, gravity) do
    weight = trunc(weight * gravity * 0.033 - 42)
    max(weight, 0)
  end
end
